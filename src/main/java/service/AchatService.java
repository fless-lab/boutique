/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.flessapps.boutique.entites.Achat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fless
 */
public class AchatService{
    
    static List<Achat> liste;
    
    public AchatService(){}
    
    public void ajouter(Achat achat){
        liste.add(achat);
    }
    
    public void modifier(Achat achat) {
        for(Achat monAchat : liste) {
            if(monAchat.getId()== achat.getId()){
                liste.set(liste.indexOf(monAchat), achat);
            }
        }
    }

    
    public Achat trouver(Integer id){
        Achat monAchat = new Achat();
        for(Achat achat : liste){
            if(achat.getId() == id){
                monAchat = achat;
            }
        }
        return monAchat;
    }
    
    
    
    public void supprimer(Integer id){
        for(Achat achat : liste){
            if(achat.getId() == id){
                liste.remove(achat);
            }
        }
    }
    
    
    public void supprimer(Achat achat){
        liste.remove(achat);
    }
    
    
    
    public List<Achat> lister(){
        return liste;
    }
    
    public List<Achat> lister(int debut, int nombre){
        List<Achat> maListe = new ArrayList<>();
        for(int i=debut;i<nombre+1;++i){
            maListe.add(liste.get(i));
        }
        return maListe;
    }
    
}

