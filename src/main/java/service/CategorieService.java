/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.flessapps.boutique.entites.Categorie;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fless
 */
public class CategorieService{
    
    static List<Categorie> liste;
    
    
    public CategorieService(){}
    
    public void ajouter(Categorie categorie){
        liste.add(categorie);
    }
    
    public void modifier(Categorie categorie) {
        for(Categorie maCategorie : liste) {
            if(maCategorie.getId()== categorie.getId()){
                liste.set(liste.indexOf(maCategorie), categorie);
            }
        }
    }

    
    public Categorie trouver(Integer id){
        Categorie maCategorie = new Categorie();
        for(Categorie categorie : liste){
            if(categorie.getId() == id){
                maCategorie = categorie;
            }
        }
        return maCategorie;
    }
    
    
    
    public void supprimer(Integer id){
        for(Categorie categorie : liste){
            if(categorie.getId() == id){
                liste.remove(categorie);
            }
        }
    }
    
    
    public void supprimer(Categorie categorie){
        liste.remove(categorie);
    }
    
    
    
    public List<Categorie> lister(){
        return liste;
    }
    
    public List<Categorie> lister(int debut, int nombre){
        List<Categorie> maListe = new ArrayList<>();
        for(int i=debut;i<nombre+1;++i){
            maListe.add(liste.get(i));
        }
        return maListe;
    }
    
}
