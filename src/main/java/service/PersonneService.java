/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.flessapps.boutique.entites.Personne;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fless
 */
public class PersonneService {
    
    static List<Personne> liste;
    
    public PersonneService(){}
    
    public void ajouter(Personne perso){
        liste.add(perso);
    }
    
    public void modifier(Personne perso) {
        for(Personne personne : liste) {
            if(personne.getId()== perso.getId()){
                liste.set(liste.indexOf(personne), perso);
            }
        }
    }

    
    public Personne trouver(Integer id){
        Personne perso = new Personne();
        for(Personne personne : liste){
            if(personne.getId() == id){
                perso = personne;
            }
        }
        return perso;
    }
    
    
    
    public void supprimer(Integer id){
        for(Personne personne : liste){
            if(personne.getId() == id){
                liste.remove(personne);
            }
        }
    }
    
    
    public void supprimer(Personne perso){
        liste.remove(perso);
    }
    
    
    
    public List<Personne> lister(){
        return liste;
    }
    
    public List<Personne> lister(int debut, int nombre){
        List<Personne> maListe = new ArrayList<>();
        for(int i=debut;i<nombre+1;++i){
            maListe.add(liste.get(i));
        }
        return maListe;
    }
}
