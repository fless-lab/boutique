/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.flessapps.boutique.entites.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fless
 */
public class GenericService<ELEMENT extends Categorie,Client,Achat,Employe,Personne,Produit,ProdiutAchete> {
    
    List<ELEMENT> liste;
    
    public GenericService(){}
    
    public GenericService(List<ELEMENT> liste){
        this.liste = liste;
    }
    
    public void ajouter(ELEMENT e){
        this.liste.add(e);
    }
    
    public void modifier(ELEMENT e){
        int id = (int) e.getId();
        this.liste.set(id,e);
    }
    
    public ELEMENT trouver(Integer id){
        return this.liste.get(id);
    }
    
    public void supprimer(Integer id){
        ELEMENT elt = this.liste.get(id);
        this.liste.remove(elt);
    }
    
    public void supprimer(ELEMENT e){
        this.liste.remove(e);
    }
    
    public List<ELEMENT> lister(){
        return this.liste;
    }
    
    public List<ELEMENT> lister(int debut, int nombre){
        List<ELEMENT> maListe = new ArrayList<>();
        for(int i=debut;i<nombre;++i){
            maListe.add(this.liste.get(i));
        }
        return maListe;
    }
}


