/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.flessapps.boutique.entites.Produit;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fless
 */
public class ProduitService {
    
    static List<Produit> liste;
    
    public ProduitService(){}
    
    public void ajouter(Produit p){
        liste.add(p);
    }
    
    public void modifier(Produit p) {
        for(Produit produit : liste) {
            if(produit.getId()== p.getId()){
                liste.set(liste.indexOf(produit), p);
            }
        }
    }

    
    public Produit trouver(Integer id){
        Produit monProduit = new Produit();
        for(Produit produit : liste){
            if(produit.getId() == id){
                monProduit = produit;
            }
        }
        return monProduit;
    }
    
    
    
    public void supprimer(Integer id){
        for(Produit produit : liste){
            if(produit.getId() == id){
                liste.remove(produit);
            }
        }
    }
    
    
    public void supprimer(Produit p){
        liste.remove(p);
    }
    
    
    
    public List<Produit> lister(){
        return liste;
    }
    
    public List<Produit> lister(int debut, int nombre){
        List<Produit> maListe = new ArrayList<>();
        for(int i=debut;i<nombre+1;++i){
            maListe.add(liste.get(i));
        }
        return maListe;
    }
}
