/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Achat;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.AchatService;

/**
 *
 * @author fless
 */
@Path("/achats")
public class AchatResource {
    
    AchatService achatService = new AchatService();
    
    @GET
    public List<Achat> listerAchat(){
        return achatService.lister();
    }
    
    @GET
    public List<Achat> listerAchat(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return achatService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{achatId}")
    public Achat trouverAchat(@PathParam("achatId") Integer achatId){
        return achatService.trouver(achatId);
    }
    
    @POST
    @Path("/{achat}")
    public void ajouterAchat(@PathParam("achat") Achat achat){
        achatService.ajouter(achat);
    }
    
    
    @PUT
    @Path("/{achat}")
    public void modifierAchat(@PathParam("achat") Achat achat){
        achatService.modifier(achat);
    }
    
    @DELETE
    @Path("/{achatId}")
    public void supprimerAchat(@PathParam("achatId")Integer achatId){
        achatService.supprimer(achatId);  
    }
    
    @DELETE
    @Path("/{achat}")
    public void supprimerAchat(@PathParam("achat") Achat achat){
        achatService.supprimer(achat);
    }
    

    
}