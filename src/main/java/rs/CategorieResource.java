/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Categorie;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.CategorieService;

/**
 *
 * @author fless
 */
@Path("/categories")
public class CategorieResource {
    
    CategorieService categorieService = new CategorieService();
    
    @GET
    public List<Categorie> listerCategorie(){
        return categorieService.lister();
    }
    
    @GET
    public List<Categorie> listerCategorie(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return categorieService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{categorieId}")
    public Categorie trouverCategorie(@PathParam("categorieId") Integer categorieId){
        return categorieService.trouver(categorieId);
    }
    
    @POST
    @Path("/{categorie}")
    public void ajouterCategorie(@PathParam("categorie") Categorie categorie){
        categorieService.ajouter(categorie);
    }
    
    
    @PUT
    @Path("/{categorie}")
    public void modifierCategorie(@PathParam("categorie") Categorie categorie){
        categorieService.modifier(categorie);
    }
    
    @DELETE
    @Path("/{categorieId}")
    public void supprimerCategorie(@PathParam("categorieId")Integer categorieId){
        categorieService.supprimer(categorieId);  
    }
    
    @DELETE
    @Path("/{categorie}")
    public void supprimerCategorie(@PathParam("categorie") Categorie categorie){
        categorieService.supprimer(categorie);
    }
    
    
}
