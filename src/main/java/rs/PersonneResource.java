/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Personne;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.PersonneService;

/**
 *
 * @author fless
 */
public class PersonneResource {
    
    PersonneService personneService = new PersonneService();
    
    @GET
    public List<Personne> listerPersonne(){
        return personneService.lister();
    }
    
    @GET
    public List<Personne> listerPersonne(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return personneService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{personneId}")
    public Personne trouverPersonne(@PathParam("personneId") Integer personneId){
        return personneService.trouver(personneId);
    }
    
    @POST
    @Path("/{personne}")
    public void ajouterPersonne(@PathParam("personne") Personne personne){
        personneService.ajouter(personne);
    }
    
    
    @PUT
    @Path("/{personne}")
    public void modifierPersonne(@PathParam("personne") Personne personne){
        personneService.modifier(personne);
    }
    
    @DELETE
    @Path("/{personneId}")
    public void supprimerPersonne(@PathParam("personneId")Integer personneId){
        personneService.supprimer(personneId);  
    }
    
    @DELETE
    @Path("/{personne}")
    public void supprimerPersonne(@PathParam("personne") Personne personne){
        personneService.supprimer(personne);
    }
}
