/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.ProduitAchete;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.ProduitAcheteService;

/**
 *
 * @author fless
 */
public class ProduitAcheteResource {
    
    ProduitAcheteService produitAcheteService = new ProduitAcheteService();
    
    @GET
    public List<ProduitAchete> listerProduitAchete(){
        return produitAcheteService.lister();
    }
    
    @GET
    public List<ProduitAchete> listerProduitAchete(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return produitAcheteService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{produitAcheteId}")
    public ProduitAchete trouverProduitAchete(@PathParam("produitAcheteId") Integer produitAcheteId){
        return produitAcheteService.trouver(produitAcheteId);
    }
    
    @POST
    @Path("/{produitAchete}")
    public void ajouterProduitAchete(@PathParam("produitAchete") ProduitAchete produitAchete){
        produitAcheteService.ajouter(produitAchete);
    }
    
    
    @PUT
    @Path("/{produitAchete}")
    public void modifierProduitAchete(@PathParam("produitAchete") ProduitAchete produitAchete){
        produitAcheteService.modifier(produitAchete);
    }
    
    @DELETE
    @Path("/{produitAcheteId}")
    public void supprimerProduitAchete(@PathParam("produitAcheteId")Integer produitAcheteId){
        produitAcheteService.supprimer(produitAcheteId);  
    }
    
    @DELETE
    @Path("/{produitAchete}")
    public void supprimerProduitAchete(@PathParam("produitAchete") ProduitAchete produitAchete){
        produitAcheteService.supprimer(produitAchete);
    }
}
