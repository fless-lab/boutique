/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Produit;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.ProduitService;

/**
 *
 * @author fless
 */
public class ProduitResource {
    
    ProduitService produitService = new ProduitService();
    
    @GET
    public List<Produit> listerProduit(){
        return produitService.lister();
    }
    
    @GET
    public List<Produit> listerProduit(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return produitService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{produitId}")
    public Produit trouverProduit(@PathParam("produitId") Integer produitId){
        return produitService.trouver(produitId);
    }
    
    @POST
    @Path("/{produit}")
    public void ajouterProduit(@PathParam("cient") Produit produit){
        produitService.ajouter(produit);
    }
    
    
    @PUT
    @Path("/{produit}")
    public void modifierProduit(@PathParam("produit") Produit produit){
        produitService.modifier(produit);
    }
    
    @DELETE
    @Path("/{produitId}")
    public void supprimerProduit(@PathParam("produitId")Integer produitId){
        produitService.supprimer(produitId);  
    }
    
    @DELETE
    @Path("/{produit}")
    public void supprimerProduit(@PathParam("produit") Produit produit){
        produitService.supprimer(produit);
    }
}
