/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;


import com.flessapps.boutique.entites.Employe;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.EmployeService;

/**
 *
 * @author fless
 */
public class EmployeResource {
    
    EmployeService employeService = new EmployeService();
    
    @GET
    public List<Employe> listerEmploye(){
        return employeService.lister();
    }
    
    @GET
    public List<Employe> listerEmploye(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return employeService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{employeId}")
    public Employe trouverEmploye(@PathParam("employeId") Integer employeId){
        return employeService.trouver(employeId);
    }
    
    @POST
    @Path("/{employe}")
    public void ajouterEmploye(@PathParam("cient") Employe employe){
        employeService.ajouter(employe);
    }
    
    
    @PUT
    @Path("/{employe}")
    public void modifierEmploye(@PathParam("employe") Employe employe){
        employeService.modifier(employe);
    }
    
    @DELETE
    @Path("/{employeId}")
    public void supprimerEmploye(@PathParam("employeId")Integer employeId){
        employeService.supprimer(employeId);  
    }
    
    @DELETE
    @Path("/{employe}")
    public void supprimerEmploye(@PathParam("empoye") Employe employe){
        employeService.supprimer(employe);
    }
    
}
