/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Client;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.ClientService;

/**
 *
 * @author fless
 */
public class ClientResource {
    
    ClientService clientService = new ClientService();
    
    @GET
    public List<Client> listerClient(){
        return clientService.lister();
    }
    
    @GET
    public List<Client> listerClient(@QueryParam("start") int debut,@QueryParam("end") int nombre){
         return clientService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{clientId}")
    public Client trouverClient(@PathParam("clientId") Integer clientId){
        return clientService.trouver(clientId);
    }
    
    @POST
    @Path("/{client}")
    public void ajouterClient(@PathParam("client") Client client){
        clientService.ajouter(client);
    }
    
    
    @PUT
    @Path("/{client}")
    public void modifierClient(@PathParam("client") Client client){
        clientService.modifier(client);
    }
    
    @DELETE
    @Path("/{clientId}")
    public void supprimerClient(@PathParam("clientId")Integer clientId){
        clientService.supprimer(clientId);  
    }
    
    @DELETE
    @Path("/{client}")
    public void supprimerClient(@PathParam("client") Client client){
        clientService.supprimer(client);
    }
    
}
