/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flessapps.boutique.entites;

/**
 *
 * @author fless
 */
import java.time.LocalDate;
import java.util.Objects;
import java.util.ArrayList;
public class Employe extends Personne{
    
    private String cnss;
    private LocalDate dateEmbauche;

    
    public Employe(){}
    
    public Employe(String cnss, LocalDate dateEmbauche, int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    @Override
    public String toString() {
        return super.toString()+"[ CNSS : "+this.cnss+" , "+this.dateEmbauche+" ]"; //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean equals(Employe autreEmploye){
        return true?autreEmploye!=null && this.getClass()==autreEmploye.getClass() && this.id==autreEmploye.id :false;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.cnss);
        hash = 89 * hash + Objects.hashCode(this.dateEmbauche);
        return hash;
    }
    
    

    
}
