/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flessapps.boutique.entites;

/**
 *
 * @author fless
 */
import java.time.LocalDate;
import java.util.Objects;
public class Client extends Personne{

    private String carteVisa;
    private String cin;

    public Client(){}
    
    public Client(String carteVisa,String cin, int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
        this.cin=cin;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.carteVisa);
        hash = 97 * hash + Objects.hashCode(this.cin);
        return hash;
    }

    //toString, equals, hashcode
    /*
    public boolean equals(Client autreClient){
    return true?autreClient!=null && this.getClass()==autreClient.getClass() && this.id==autreClient.id :false;
    }
    @Override
    public String toString() {
    return super.toString() + "[ Carte visa :"+this.carteVisa+" ]";
    }
    @Override
    public int hashCode() {
    int hash = 7;
    hash = 47 * hash + Objects.hashCode(this.carteVisa);
    return hash;
    }*/
    public boolean equals(Client autreClient){
        return true?autreClient!=null && this.getClass()==autreClient.getClass() && this.id==autreClient.id :false;
    }

    @Override
    public String toString() {
        return super.toString() + "[ Carte visa :"+this.carteVisa+", CIN : "+this.cin+"]";
    }
    
    
    
    
 
}
