/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flessapps.boutique.entites;

/**
 *
 * @author fless
 */
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
public class Personne implements Serializable{
    
    protected int   id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;
    
    public Personne(){}
    
    public Personne(int id,String nom,String prenom,LocalDate dateNaissance){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }
    
    
    public int getAge(){
        return this.dateNaissance.getYear()<LocalDate.now().getYear()?LocalDate.now().getYear() - this.dateNaissance.getYear():null;
    }
    
    public int getAge(LocalDate date){
       return this.dateNaissance.getYear()<date.getYear()?date.getYear() - this.dateNaissance.getYear():null;
    }
    
    public int getId(){
        return this.id;
    }
    
    public String getNom(){
        return this.nom;
    }
    
    public String getPrenom(){
        return this.prenom;
    }
    
    public void setId(int Id){
        this.id = id;
    }
    public void setNom(String nom){
        this.nom = nom;
    }
    public void setPrenom(String prenom){
        this.prenom = prenom;
    }
    public void setDateNaissance(LocalDate dateNaissance){
        this.dateNaissance = dateNaissance;
    }
    
    
    //toString, equals, hashcode
    /*
    
    public String toString(){
        return this.nom+"  "+this.prenom+"  N°"+this.id+"( né le "+this.dateNaissance+")";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + this.id;
        hash = 17 * hash + Objects.hashCode(this.nom);
        hash = 17 * hash + Objects.hashCode(this.prenom);
        hash = 17 * hash + Objects.hashCode(this.dateNaissance);
        return hash;
    }
    */
    
    public boolean equals(Personne autrePersonne){
        return true?autrePersonne!=null && this.getClass()==autrePersonne.getClass() && this.id==autrePersonne.id :false;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id;
        hash = 53 * hash + Objects.hashCode(this.nom);
        hash = 53 * hash + Objects.hashCode(this.prenom);
        hash = 53 * hash + Objects.hashCode(this.dateNaissance);
        return hash;
    }

    @Override
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + '}';
    }
    
    
    
    
}
